export default {
    user: {
        role: 'superAdmin',
        username: 'q',
        password: 'q',
        email: 'qq22337383@gmail.com',
        nickname: 'VueBlog',
        motto: 'Never too old to learn',
        avatar: 'avatar.png'
    },
    jwt: {
        secret: 'vueblog'
    },
    mongodb: {
        host: '127.0.0.1',
        database: 'vueblog',
        port: 27017,
        username: '',
        password: ''
    },
    production: {
        host: '120.79.46.194',
        domain: 'http://www.caozx.vip:8081'
            // domain: 'http://127.0.0.1:3000'
    },
    app: {
        host: '127.0.0.1',
        port: 3000,
        routerBaseApi: '/api'
    }
}